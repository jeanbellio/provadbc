FROM java:8-jdk-alpine

COPY ./target/teste-dbc-0.0.1-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch teste-dbc-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","teste-dbc-0.0.1-SNAPSHOT.jar"]  