package com.provadbc.api.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.provadbc.api.document.Marca;
import com.provadbc.api.service.MarcaService;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class MarcaResource {

	@Autowired
	private MarcaService marcaService;

	@GetMapping("/marcas")
	public ResponseEntity<List<Marca>> listaMarcas() {
		List<Marca> marcas = new ArrayList<>();
		marcas = marcaService.buscaMarcasTabelaFIPE();
		marcas = marcaService.findAll();
		if(marcas.size() > 0) {
			return ResponseEntity.ok(marcas);
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
}