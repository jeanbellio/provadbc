package com.provadbc.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.provadbc.api.document.Marca;
import com.provadbc.api.integration.TabelaFIPEClient;
import com.provadbc.api.repository.MarcaRepository;

@Service
public class MarcaService {

	@Autowired
	MarcaRepository marcaRepository;

	@Autowired
	TabelaFIPEClient clientTabelaFIPE;
	
	public List<Marca> buscaMarcasTabelaFIPE() {
		List<Marca> marcas = new ArrayList<>();
		marcas = clientTabelaFIPE.buscaMarcasFipeApi();
		if(marcas.size() > 0) {
			return this.salvaMarcas(marcas);
		} else {
			return marcas;
		}
	}

	public List<Marca> salvaMarcas(List<Marca> marcas) {
		return this.marcaRepository.saveAll(marcas);
	}
	
	public List<Marca> findAll() {
		return this.marcaRepository.findAll();
	}
}
