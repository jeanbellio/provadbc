package com.provadbc.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.provadbc.api.document.Marca;

@Repository
public interface MarcaRepository extends MongoRepository<Marca, String>{

}
