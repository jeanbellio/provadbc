package com.provadbc.api.integration;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.provadbc.api.document.Marca;

@FeignClient(value = "marcas", url = "http://fipeapi.appspot.com")
public interface TabelaFIPEClient {
	
	@RequestMapping(method = RequestMethod.GET, value = "/api/1/carros/marcas.json")
	List<Marca> buscaMarcasFipeApi();

}
