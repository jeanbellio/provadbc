package com.provadbc.api.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "marca")
public class Marca {

	@Id
	private String id;

	private String name;

	private String fipe_name;

	private String key;

	private long order;

	public Marca() {
		super();
	}

	public Marca(String id, String name, String fipe_name, String key, long order) {
		super();
		this.id = id;
		this.name = name;
		this.fipe_name = fipe_name;
		this.key = key;
		this.order = order;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFipe_name() {
		return fipe_name;
	}

	public void setFipe_name(String fipe_name) {
		this.fipe_name = fipe_name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public long getOrder() {
		return order;
	}

	public void setOrder(long order) {
		this.order = order;
	}

}
